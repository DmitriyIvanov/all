﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace PracticalProject
{
    public partial class Account : Form
    {
        string Role;
        string Phone, Email, Id_Riel, Id_Client, id_type;
        public Account(string role, string fio)
        {
            InitializeComponent();
            label1.Text = fio;
            Role = role;
            using (SqlConnection sqlConnection3 = new SqlConnection(connect))
            {
                sqlDataAdapter = new SqlDataAdapter("select Код_потребности as Идентификатор,(Клиенты.Фамилия + ' ' + Клиенты.Имя + ' ' + Клиенты.Отчество) as Клиент, (Риэлторы.Фамилия + ' ' + Риэлторы.Имя + ' ' + Риэлторы.Отчество) as Риэлтор, Типы_объектов.Наименование as Объект, Минимальная_стоимость,Максимальная_стоимость,Минимальная_площадь,Максимальная_площадь,Минимальное_количество_комнат,Максиамальное_количество_комнат,Минимальный_этаж,Максимальный_этаж from Потребности join Клиенты on Потребности.Код_клиента = Клиенты.Код_клиента join Риэлторы on Риэлторы.Код_риэлтора = Потребности.Код_риэлтора join Типы_объектов on Типы_объектов.Код_типа_объектов = Потребности.Код_типа_объектов", sqlConnection3);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                dataGridView1.DataSource = dataSet.Tables[0];
                sqlDataAdapter = new SqlDataAdapter("select Код_предложения as Идентификатор,(Клиенты.Фамилия + ' ' + Клиенты.Имя + ' ' + Клиенты.Отчество) as Клиент, (Риэлторы.Фамилия + ' ' + Риэлторы.Имя + ' ' + Риэлторы.Отчество) as Риэлтор, Стоимость, Общая_площадь, Количество_комнат, Количество_этажей, Этаж, Свидетельство from Предложения join Клиенты on Предложения.Код_клиента = Клиенты.Код_клиента join Риэлторы on Риэлторы.Код_риэлтора = Предложения.Код_риэлтора join Объекты_недвижимости on Объекты_недвижимости.Код_объекта = Предложения.Код_объекта", sqlConnection3);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                dataGridView2.DataSource = dataSet.Tables[0];
            }
            using (SqlConnection sqlConnection2 = new SqlConnection(connect))
            {
                sqlDataAdapter = new SqlDataAdapter("select Фамилия, Имя, Отчество from Риэлторы",sqlConnection2);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    comboBox4.Items.Add(dataSet.Tables[0].Rows[i][0].ToString() + " " + dataSet.Tables[0].Rows[i][1].ToString() + " " + dataSet.Tables[0].Rows[i][2].ToString());
                    comboBox6.Items.Add(dataSet.Tables[0].Rows[i][0].ToString() + " " + dataSet.Tables[0].Rows[i][1].ToString() + " " + dataSet.Tables[0].Rows[i][2].ToString());
                }
            }
            using (SqlConnection sqlConnection1 = new SqlConnection(connect))
            {
                sqlDataAdapter = new SqlDataAdapter("select Наименование, Код_типа_объектов from Типы_объектов", sqlConnection1);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    comboBox2.Items.Add(dataSet.Tables[0].Rows[i][0]);
                    comboBox5.Items.Add(dataSet.Tables[0].Rows[i][0]);
                }
                sqlDataAdapter = new SqlDataAdapter("select Наименование, Код_района from Районы", sqlConnection1);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    comboBox3.Items.Add(dataSet.Tables[0].Rows[i][0]);
                }
            }
            if (role == "Риэлтор")
            {
                button4.Visible = false;
                button5.Visible = false;
                
                label8.Visible = true;
                numericUpDown1.Visible = true;
                using (SqlConnection sqlConnection = new SqlConnection(connect))
                {
                    sqlDataAdapter = new SqlDataAdapter("select Фамилия, Имя, Отчество, Доля_сделки, Номер_телефона, Email, Пароль, Код_риэлтора from Риэлторы", sqlConnection);
                    dataSet = new DataSet();
                    sqlDataAdapter.Fill(dataSet);
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        if (fio == dataSet.Tables[0].Rows[i][0].ToString() + "\n" + dataSet.Tables[0].Rows[i][1].ToString() + "\n" + dataSet.Tables[0].Rows[i][2].ToString())
                        {
                            textBox1.Text = dataSet.Tables[0].Rows[i][0].ToString();
                            textBox2.Text = dataSet.Tables[0].Rows[i][1].ToString();
                            textBox3.Text = dataSet.Tables[0].Rows[i][2].ToString();
                            numericUpDown1.Value = Convert.ToInt32(dataSet.Tables[0].Rows[i][3]);
                            textBox4.Text = dataSet.Tables[0].Rows[i][4].ToString();
                            
                            if (textBox5.Text == "" && comboBox1.Text == "")
                            {
                                for (int j = 0; j < dataSet.Tables[0].Rows[i][5].ToString().Length; j++)
                                {
                                    textBox5.Text = dataSet.Tables[0].Rows[i][5].ToString().Substring(0, dataSet.Tables[0].Rows[i][5].ToString().IndexOf('@'));
                                }
                                comboBox1.Text = dataSet.Tables[0].Rows[i][5].ToString().Remove(0, dataSet.Tables[0].Rows[i][5].ToString().IndexOf('@')) + "";
                                comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
                            }
                            else
                            {
                                comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
                                textBox5.Text = dataSet.Tables[0].Rows[i][5].ToString();
                            }
                            Id_Riel = dataSet.Tables[0].Rows[i][7].ToString();
                            break;
                        }
                    }
                    surname = textBox1.Text;
                    name = textBox2.Text;
                    lastname = textBox3.Text;
                    phone = textBox4.Text;
                    mail = textBox5.Text + comboBox1.Text;
                    share = numericUpDown1.Value.ToString();
                }
            }
            if (role == "Клиент")
            {
                button3.Visible = false;
                button9.Visible = false;
                using (SqlConnection sqlConnection = new SqlConnection(connect))
                {
                    sqlDataAdapter = new SqlDataAdapter("select Фамилия, Имя, Отчество, Номер_телефона, Email, Пароль, Код_клиента from Клиенты", sqlConnection);
                    dataSet = new DataSet();
                    sqlDataAdapter.Fill(dataSet);
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        if (fio == dataSet.Tables[0].Rows[i][0].ToString() + "\n" + dataSet.Tables[0].Rows[i][1].ToString() + "\n" + dataSet.Tables[0].Rows[i][2].ToString())
                        {
                            textBox1.Text = dataSet.Tables[0].Rows[i][0].ToString();
                            textBox2.Text = dataSet.Tables[0].Rows[i][1].ToString();
                            textBox3.Text = dataSet.Tables[0].Rows[i][2].ToString();
                            textBox4.Text = dataSet.Tables[0].Rows[i][3].ToString();
                            
                            if (textBox5.Text == "" && comboBox1.Text == "")
                            {
                                for (int j = 0; j < dataSet.Tables[0].Rows[i][4].ToString().Length; j++)
                                {
                                    textBox5.Text = dataSet.Tables[0].Rows[i][4].ToString().Substring(0, dataSet.Tables[0].Rows[i][4].ToString().IndexOf('@'));
                                }
                                comboBox1.Text = dataSet.Tables[0].Rows[i][4].ToString().Remove(0, dataSet.Tables[0].Rows[i][4].ToString().IndexOf('@'));
                                comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
                            }
                            else
                            {
                                comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
                                textBox5.Text = dataSet.Tables[0].Rows[i][4].ToString();
                            }
                            Id_Client = dataSet.Tables[0].Rows[i][6].ToString();
                            break;
                        }
                    }
                    surname = textBox1.Text;
                    name = textBox2.Text;
                    lastname = textBox3.Text;
                    phone = textBox4.Text;
                    mail = textBox5.Text + comboBox1.Text;
                }
            }
        }
        string connect = ConnectSQL.Connect();
        SqlDataAdapter sqlDataAdapter;
        DataSet dataSet;
        SqlCommand sqlCommand;

        private void button1_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage1;
        }
        private void button3_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage5;
        } 
        string surname, name, lastname, phone, mail, share;
        string id_rayon;
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            using (SqlConnection sqlConnection1 = new SqlConnection(connect))
            {
                sqlDataAdapter = new SqlDataAdapter($"select Наименование, Код_типа_объектов from Типы_объектов", sqlConnection1);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    if (comboBox2.Text == dataSet.Tables[0].Rows[i][0].ToString())
                    {
                        id_type = dataSet.Tables[0].Rows[i][1].ToString();
                    }

                }
                if (comboBox2.Text == "Квартира")
                {
                    groupBox2.Visible = true;
                    groupBox3.Visible = false;
                    groupBox4.Visible = true;
                }
                if (comboBox2.Text == "Дом")
                {
                    groupBox2.Visible = false;
                    groupBox3.Visible = true;
                    groupBox4.Visible = true;
                }
                if (comboBox2.Text == "Земля")
                {
                    groupBox2.Visible = false;
                    groupBox3.Visible = false;
                    groupBox4.Visible = false;
                }
            }
        }

        private void textBox9_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void button9_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage2;
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (SqlConnection sqlConnection1 = new SqlConnection(connect))
            {
                sqlDataAdapter = new SqlDataAdapter("select Наименование, Код_района from Районы", sqlConnection1);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    if (comboBox3.Text == dataSet.Tables[0].Rows[i][0].ToString())
                    {
                        id_rayon = dataSet.Tables[0].Rows[i][1].ToString();
                    }

                }
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage3;
        }

        private void textBox14_KeyPress(object sender, KeyPressEventArgs e)
        {
            char Word = e.KeyChar;
            if (!Char.IsDigit(Word) && Word != 8)
            {
                e.Handled = true;
            }
        }

        private void textBox17_KeyPress(object sender, KeyPressEventArgs e)
        {
            char Word = e.KeyChar;
            if (!Char.IsDigit(Word) && Word != 8)
            {
                e.Handled = true;
            }
        }

        private void textBox18_KeyPress(object sender, KeyPressEventArgs e)
        {
            char Word = e.KeyChar;
            if (!Char.IsDigit(Word) && Word != 8)
            {
                e.Handled = true;
            }
        }
        string idSotrud,idClient,idObject;

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        string idTobject,idtClient,idtrIEL;
        private void button12_Click(object sender, EventArgs e)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connect))
            {
                sqlDataAdapter = new SqlDataAdapter("select Код_типа_объектов, Наименование from Типы_объектов", sqlConnection);
                    dataSet = new DataSet();
                    sqlDataAdapter.Fill(dataSet);
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        if (comboBox5.Text == dataSet.Tables[0].Rows[i][1].ToString())
                        {
                            idTobject = dataSet.Tables[0].Rows[i][0].ToString();
                        }
                    }
                sqlDataAdapter = new SqlDataAdapter("select Код_клиента, Фамилия, Имя, Отчество from Клиенты", sqlConnection);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    if (label1.Text == dataSet.Tables[0].Rows[i][1].ToString() + "\n" + dataSet.Tables[0].Rows[i][2].ToString() + "\n" + dataSet.Tables[0].Rows[i][3].ToString())
                    {
                        idtClient = dataSet.Tables[0].Rows[i][0].ToString();
                    }
                }
                sqlDataAdapter = new SqlDataAdapter("select Код_риэлтора, Фамилия, Имя, Отчество from Риэлторы", sqlConnection);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    if (comboBox6.Text == dataSet.Tables[0].Rows[i][1].ToString() + " " + dataSet.Tables[0].Rows[i][2].ToString() + " " + dataSet.Tables[0].Rows[i][3].ToString())
                    {
                        idtrIEL = dataSet.Tables[0].Rows[i][0].ToString();
                    }
                }
                if (comboBox5.SelectedIndex > -1 && textBox19.Text != "" && textBox20.Text != "" && comboBox6.SelectedIndex > -1 && textBox21.Text != "" && textBox22.Text != "" && textBox23.Text != "" && textBox24.Text != "" && textBox25.Text != "" && textBox26.Text != "")
                {
                    if (Convert.ToInt32(textBox20.Text) > Convert.ToInt32(textBox19.Text) && Convert.ToInt32(textBox22.Text) > Convert.ToInt32(textBox21.Text) && Convert.ToInt32(textBox24.Text) > Convert.ToInt32(textBox23.Text) && Convert.ToInt32(textBox26.Text) > Convert.ToInt32(textBox25.Text))
                    {
                        sqlCommand = new SqlCommand("insert into Потребности(Минимальная_стоимость, Максимальная_стоимость, Код_клиента, Код_риэлтора, Минимальная_площадь, Максимальная_площадь, Минимальное_количество_комнат, Максиамальное_количество_комнат, Минимальный_этаж, Максимальный_этаж, Код_типа_объектов) values (@minCos, @maxCos, @kodclient, @kodriel, @minS, @maxS, @minRoom, @maxRoom, @minAt, @maxAt, @kodObj)", sqlConnection);
                        sqlCommand.Parameters.AddWithValue("@minCos", textBox19.Text);
                        sqlCommand.Parameters.AddWithValue("@maxCos", textBox20.Text);
                        sqlCommand.Parameters.AddWithValue("@kodclient", idtClient);
                        sqlCommand.Parameters.AddWithValue("@kodriel", idtrIEL);
                        sqlCommand.Parameters.AddWithValue("@minS", textBox21.Text);
                        sqlCommand.Parameters.AddWithValue("@maxS", textBox22.Text);
                        sqlCommand.Parameters.AddWithValue("@minRoom", textBox23.Text);
                        sqlCommand.Parameters.AddWithValue("@maxRoom", textBox24.Text);
                        sqlCommand.Parameters.AddWithValue("@minAt", textBox25.Text);
                        sqlCommand.Parameters.AddWithValue("@maxAt", textBox26.Text);
                        sqlCommand.Parameters.AddWithValue("@kodObj", idTobject);
                        sqlConnection.Open();
                        try
                        {
                            sqlCommand.ExecuteNonQuery();
                            MessageBox.Show("Вы добавили запись");
                            Account account = new Account(Role, label1.Text);
                            this.Hide();
                            account.Show();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                    }
                    else
                    {
                        MessageBox.Show("Вы некорректно ввели данные!");
                    }
                    
                }
            }
        }

        private void textBox19_KeyPress(object sender, KeyPressEventArgs e)
        {
            char Word = e.KeyChar;
            if (!Char.IsDigit(Word) && Word != 8)
            {
                e.Handled = true;
            }
        }

        private void textBox20_KeyPress(object sender, KeyPressEventArgs e)
        {
            char Word = e.KeyChar;
            if (!Char.IsDigit(Word) && Word != 8)
            {
                e.Handled = true;
            }
        }

        private void tabPage4_Click(object sender, EventArgs e)
        {

        }

        private void textBox23_TextChanged(object sender, EventArgs e)
        {
        }

        private void textBox24_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox23_KeyPress(object sender, KeyPressEventArgs e)
        {
            char Word = e.KeyChar;
            if (!Char.IsDigit(Word) && Word != 8)
            {
                e.Handled = true;
            }
        }

        private void textBox24_KeyPress(object sender, KeyPressEventArgs e)
        {
            char Word = e.KeyChar;
            if (!Char.IsDigit(Word) && Word != 8)
            {
                e.Handled = true;
            }
        }

        private void textBox25_KeyPress(object sender, KeyPressEventArgs e)
        {
            char Word = e.KeyChar;
            if (!Char.IsDigit(Word) && Word != 8)
            {
                e.Handled = true;
            }
        }

        private void textBox26_KeyPress(object sender, KeyPressEventArgs e)
        {
            char Word = e.KeyChar;
            if (!Char.IsDigit(Word) && Word != 8)
            {
                e.Handled = true;
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {

            using (SqlConnection sqlConnection = new SqlConnection(connect))
            {
                
                sqlCommand = new SqlCommand("insert into Сделки (Код_потребности, Код_предложения) values (@kodPot, @kodPred)", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@kodPot", dataGridView1.SelectedCells[0].Value);
                sqlCommand.Parameters.AddWithValue("@kodPred", dataGridView2.SelectedCells[0].Value);
                sqlConnection.Open();
                try
                {
                    sqlCommand.ExecuteNonQuery();
                    MessageBox.Show("Сделка в работе!");
                    Account account = new Account(Role, label1.Text);
                    this.Hide();
                    account.Show();
                }
                catch
                {
                    MessageBox.Show("Произошла ошибка!");
                }
            }
        }

        private void textBox21_KeyPress(object sender, KeyPressEventArgs e)
        {
            char Word = e.KeyChar;
            if (!Char.IsDigit(Word) && Word != 8 && Word != 46)
            {
                e.Handled = true;
            }
        }

        private void textBox22_KeyPress(object sender, KeyPressEventArgs e)
        {
            char Word = e.KeyChar;
            if (!Char.IsDigit(Word) && Word != 8 && Word != 46)
            {
                e.Handled = true;
            }
        }

        private void comboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connect))
            {
                if (textBox17.Text != "" && textBox18.Text != "" && comboBox4.SelectedIndex > -1)
                {
                    sqlDataAdapter = new SqlDataAdapter("select Код_риэлтора, Фамилия, Имя, Отчество from Риэлторы", sqlConnection);
                    dataSet = new DataSet();
                    sqlDataAdapter.Fill(dataSet);
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        if (comboBox4.Text == dataSet.Tables[0].Rows[i][1].ToString() + " " + dataSet.Tables[0].Rows[i][2].ToString() + " " + dataSet.Tables[0].Rows[i][3].ToString())
                        {
                            idSotrud = dataSet.Tables[0].Rows[i][0].ToString();
                        }
                    }
                    sqlDataAdapter = new SqlDataAdapter("select Код_объекта, Свидетельство from Объекты_недвижимости", sqlConnection);
                    dataSet = new DataSet();
                    sqlDataAdapter.Fill(dataSet);
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        if (textBox17.Text == dataSet.Tables[0].Rows[i][1].ToString())
                        {
                            idObject = dataSet.Tables[0].Rows[i][0].ToString();
                        }
                    }
                    sqlDataAdapter = new SqlDataAdapter("select Код_клиента, Фамилия, Имя, Отчество from Клиенты", sqlConnection);
                    dataSet = new DataSet();
                    sqlDataAdapter.Fill(dataSet);
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        if (label1.Text == dataSet.Tables[0].Rows[i][1].ToString() + "\n" + dataSet.Tables[0].Rows[i][2].ToString() + "\n" + dataSet.Tables[0].Rows[i][3].ToString())
                        {
                            idClient = dataSet.Tables[0].Rows[i][0].ToString();
                        }
                    }
                        sqlCommand = new SqlCommand("insert into Предложения(Стоимость, Код_риэлтора, Код_клиента, Код_объекта) values (@stoim, @kod_riel, @kod_klient, @kod_obj)", sqlConnection);
                        sqlCommand.Parameters.AddWithValue("@stoim",textBox18.Text);
                        sqlCommand.Parameters.AddWithValue("@kod_riel", idSotrud);
                        sqlCommand.Parameters.AddWithValue("@kod_klient",idClient);
                        sqlCommand.Parameters.AddWithValue("@kod_obj", idObject);
                        sqlConnection.Open();
                        try
                        {
                            sqlCommand.ExecuteNonQuery();
                            MessageBox.Show("Данные добавлены!");
                            Account account = new Account(Role, label1.Text);
                            this.Hide();
                            account.Show();
                        }
                        catch(Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                    
                }
                else
                {
                    MessageBox.Show("Вы заполнили не все поля!");
                }
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (comboBox2.SelectedIndex > -1)
            {

                using (SqlConnection sqlConnection = new SqlConnection(connect))
                {
                    if (comboBox2.Text == "Квартира")
                    {
                        if (textBox6.Text != "" && textBox11.Text != "" && textBox7.Text !="" && textBox15.Text !="" && textBox16.Text != "" && textBox8.Text != "" && textBox9.Text != "" && textBox10.Text != "" && textBox12.Text != "" && comboBox3.SelectedIndex > -1 && textBox14.Text != "")
                        {
                            sqlCommand = new SqlCommand("insert into Объекты_недвижимости(Город, Улица, Номер_дома, Квартира, Код_района, Код_типа_объектов, Широта, Долгота, Общая_площадь, Количество_комнат, Этаж, Свидетельство) values (@gorod, @ulitsa, @nom_doma, @kvar, @kodr, @kodt, @shir, @dol,@obsch, @kolkom, @at, @svid)", sqlConnection);
                            sqlCommand.Parameters.AddWithValue("@gorod", textBox6.Text);
                            sqlCommand.Parameters.AddWithValue("@ulitsa", textBox7.Text);
                            sqlCommand.Parameters.AddWithValue("@nom_doma", textBox15.Text);
                            sqlCommand.Parameters.AddWithValue("@kvar", textBox16.Text);
                            sqlCommand.Parameters.AddWithValue("@kodr", id_rayon);
                            sqlCommand.Parameters.AddWithValue("@kodt", id_type);
                            sqlCommand.Parameters.AddWithValue("@shir", textBox9.Text);
                            sqlCommand.Parameters.AddWithValue("@dol", textBox8.Text);
                            sqlCommand.Parameters.AddWithValue("@obsch", textBox10.Text);
                            sqlCommand.Parameters.AddWithValue("@kolkom", textBox12.Text);
                            sqlCommand.Parameters.AddWithValue("@at", textBox11.Text);
                            sqlCommand.Parameters.AddWithValue("@svid", textBox14.Text);
                            sqlConnection.Open();
                            try
                            {
                                sqlCommand.ExecuteNonQuery();
                                MessageBox.Show("Вы добавили запись");
                                Account account = new Account(Role, label1.Text);
                                this.Hide();
                                account.Show();
                            }
                            catch
                            {
                                MessageBox.Show("Добавить запись не удалось!");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Вы заполнили не все данные для 'Квартира'");
                        }

                    }
                    if (comboBox2.Text == "Дом")
                    {
                        if (textBox6.Text != "" && textBox7.Text != "" && textBox15.Text != "" && textBox13.Text != "" && textBox8.Text != "" && textBox9.Text != "" && textBox10.Text != "" && textBox12.Text != "" && comboBox3.SelectedIndex > -1 && textBox14.Text != "")
                        {
                            sqlCommand = new SqlCommand("insert into Объекты_недвижимости(Город, Улица, Номер_дома, Код_района, Код_типа_объектов, Широта, Долгота, Общая_площадь, Количество_комнат, Количество_этажей, Свидетельство) values (@gorod, @ulitsa, @nom_doma, @kodr, @kodt, @shir, @dol,@obsch, @kolkom, @kolat, @svid)", sqlConnection);
                            sqlCommand.Parameters.AddWithValue("@gorod", textBox6.Text);
                            sqlCommand.Parameters.AddWithValue("@ulitsa", textBox7.Text);
                            sqlCommand.Parameters.AddWithValue("@nom_doma", textBox15.Text);
                            sqlCommand.Parameters.AddWithValue("@kodr", id_rayon);
                            sqlCommand.Parameters.AddWithValue("@kodt", id_type);
                            sqlCommand.Parameters.AddWithValue("@shir", textBox9.Text);
                            sqlCommand.Parameters.AddWithValue("@dol", textBox8.Text);
                            sqlCommand.Parameters.AddWithValue("@obsch", textBox10.Text);
                            sqlCommand.Parameters.AddWithValue("@kolkom", textBox12.Text);
                            sqlCommand.Parameters.AddWithValue("@kolat", textBox13.Text);
                            sqlCommand.Parameters.AddWithValue("@svid", textBox14.Text);
                            sqlConnection.Open();
                            try
                            {
                                sqlCommand.ExecuteNonQuery();
                                MessageBox.Show("Вы добавили запись");
                                Account account = new Account(Role, label1.Text);
                                this.Hide();
                                account.Show();
                            }
                            catch
                            {
                                MessageBox.Show("Добавить запись не удалось!");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Вы заполнили не все данные для 'Дом'");
                        }
                    }
                    if (comboBox2.Text == "Земля")
                    {
                        if (textBox6.Text != "" && textBox7.Text != "" && textBox15.Text != "" && textBox8.Text != "" && textBox9.Text != "" && textBox10.Text != "" && comboBox3.SelectedIndex > -1 && textBox14.Text != "")
                        {
                            sqlCommand = new SqlCommand("insert into Объекты_недвижимости(Город, Улица, Номер_дома, Код_района, Код_типа_объектов, Широта, Долгота, Общая_площадь, Свидетельство) values (@gorod, @ulitsa, @nom_doma, @kodr, @kodt, @shir, @dol,@obsch, @svid)", sqlConnection);
                            sqlCommand.Parameters.AddWithValue("@gorod", textBox6.Text);
                            sqlCommand.Parameters.AddWithValue("@ulitsa", textBox7.Text);
                            sqlCommand.Parameters.AddWithValue("@nom_doma", textBox15.Text);
                            sqlCommand.Parameters.AddWithValue("@kodr", id_rayon);
                            sqlCommand.Parameters.AddWithValue("@kodt", id_type);
                            sqlCommand.Parameters.AddWithValue("@shir", textBox9.Text);
                            sqlCommand.Parameters.AddWithValue("@dol", textBox8.Text);
                            sqlCommand.Parameters.AddWithValue("@obsch", textBox10.Text);
                            sqlCommand.Parameters.AddWithValue("@svid", textBox14.Text);
                            sqlConnection.Open();
                            try
                            {
                                sqlCommand.ExecuteNonQuery();
                                MessageBox.Show("Вы добавили запись");
                                Account account = new Account(Role, label1.Text);
                                this.Hide();
                                account.Show();
                            }
                            catch
                            {
                                MessageBox.Show("Добавить запись не удалось!");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Вы заполнили не все данные для 'Земля'");
                        }
                            
                    }

                   

                }
            }
            else
            {
                MessageBox.Show("Вы не выбрали объект для просмотра!");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage4;
        }

        UpdatePassword updatePassword;
        private void button8_Click(object sender, EventArgs e)
        {
            if (Role == "Риэлтор")
            {
                updatePassword = new UpdatePassword(Role, label1.Text, Id_Riel);
                this.Hide();
                updatePassword.Show();
            }
            if (Role == "Клиент")
            {
                updatePassword = new UpdatePassword(Role, label1.Text, Id_Client);
                this.Hide();
                updatePassword.Show();
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
                if (Role == "Риэлтор")
                {
                    if (textBox1.Text != surname || textBox2.Text != name || textBox3.Text != lastname || textBox4.Text != phone || textBox5.Text+comboBox1.Text != mail || numericUpDown1.Value.ToString() != share)
                    {
                        if (textBox1.Text != "")
                        {
                            using (SqlConnection sqlConnection = new SqlConnection(connect))
                            {
                                sqlDataAdapter = new SqlDataAdapter($"update Риэлторы set Фамилия = '{textBox1.Text}' where Код_риэлтора = '{Id_Riel}'", sqlConnection);
                                dataSet = new DataSet();
                                sqlDataAdapter.Fill(dataSet);
                            }                            
                        }
                        if (textBox2.Text != "")
                        {
                            using (SqlConnection sqlConnection = new SqlConnection(connect))
                            {
                                sqlDataAdapter = new SqlDataAdapter($"update Риэлторы set Имя = '{textBox2.Text}' where Код_риэлтора = '{Id_Riel}'", sqlConnection);
                                dataSet = new DataSet();
                                sqlDataAdapter.Fill(dataSet);
                            }
                        }
                        if (textBox3.Text != "")
                        {
                            using (SqlConnection sqlConnection = new SqlConnection(connect))
                            {
                                sqlDataAdapter = new SqlDataAdapter($"update Риэлторы set Отчество = '{textBox3.Text}' where Код_риэлтора = '{Id_Riel}'", sqlConnection);
                                dataSet = new DataSet();
                                sqlDataAdapter.Fill(dataSet);
                            }
                        }
                        if (textBox4.Text != "")
                        {
                            using (SqlConnection sqlConnection = new SqlConnection(connect))
                            {
                                sqlDataAdapter = new SqlDataAdapter($"update Риэлторы set Номер_телефона = '{textBox4.Text}' where Код_риэлтора = '{Id_Riel}'", sqlConnection);
                                dataSet = new DataSet();
                                sqlDataAdapter.Fill(dataSet);
                            }                           
                        }
                        if (textBox5.Text+comboBox1.Text != "")
                        {
                            using (SqlConnection sqlConnection = new SqlConnection(connect))
                            {
                                sqlDataAdapter = new SqlDataAdapter($"update Риэлторы set Email = '{textBox5.Text + comboBox1.Text}' where Код_риэлтора = '{Id_Riel}'", sqlConnection);
                                dataSet = new DataSet();
                                sqlDataAdapter.Fill(dataSet);
                            }
                        }
                        if (numericUpDown1.Value.ToString() != "")
                        {
                            using (SqlConnection sqlConnection = new SqlConnection(connect))
                            {
                            sqlDataAdapter = new SqlDataAdapter($"update Риэлторы set Доля_сделки = {numericUpDown1.Value} where Код_риэлтора = '{Id_Riel}'", sqlConnection);
                            dataSet = new DataSet();
                            sqlDataAdapter.Fill(dataSet);
                            }
                        }
                            
                    MessageBox.Show("Данные обновлены!");
                    Account account = new Account(Role, label1.Text);
                    this.Hide();
                    account.Show();
                     
                    }
                    else
                    {
                        MessageBox.Show("Вы не произвели изменений!");
                    }
                }
                if (Role == "Клиент")
                {
                if (textBox1.Text != surname || textBox2.Text != name || textBox3.Text != lastname || textBox4.Text != phone || textBox5.Text + comboBox1.Text != mail || numericUpDown1.Value.ToString() != share)
                {
                    if (textBox1.Text != "")
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(connect))
                        {
                            sqlDataAdapter = new SqlDataAdapter($"update Клиенты set Фамилия = '{textBox1.Text}' where Код_клиента = '{Id_Client}'", sqlConnection);
                            dataSet = new DataSet();
                            sqlDataAdapter.Fill(dataSet);
                        }
                    }
                    if (textBox2.Text != "")
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(connect))
                        {
                            sqlDataAdapter = new SqlDataAdapter($"update Клиенты set Имя = '{textBox2.Text}' where Код_клиента = '{Id_Client}'", sqlConnection);
                            dataSet = new DataSet();
                            sqlDataAdapter.Fill(dataSet);
                        }
                    }
                    if (textBox3.Text != "")
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(connect))
                        {
                            sqlDataAdapter = new SqlDataAdapter($"update Клиенты set Отчество = '{textBox3.Text}' where Код_клиента = '{Id_Client}'", sqlConnection);
                            dataSet = new DataSet();
                            sqlDataAdapter.Fill(dataSet);
                        }
                    }
                    if (textBox4.Text != "")
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(connect))
                        {
                            sqlDataAdapter = new SqlDataAdapter($"update Клиенты set Номер_телефона = '{textBox4.Text}' where Код_клиента = '{Id_Client}'", sqlConnection);
                            dataSet = new DataSet();
                            sqlDataAdapter.Fill(dataSet);
                        }
                    }
                    if (textBox5.Text + comboBox1.Text != "")
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(connect))
                        {
                            sqlDataAdapter = new SqlDataAdapter($"update Клиенты set Email = '{textBox5.Text + comboBox1.Text}' where Код_клиента = '{Id_Client}'", sqlConnection);
                            dataSet = new DataSet();
                            sqlDataAdapter.Fill(dataSet);
                        }
                    }
                    MessageBox.Show("Данные обновлены!");
                    Account account = new Account(Role, label1.Text);
                    this.Hide();
                    account.Show();

                }
                else
                {
                    MessageBox.Show("Вы не произвели изменений!");
                }
            } 
        }
        private void button7_Click(object sender, EventArgs e)
        {
            Phone = textBox4.Text;
            Email = textBox5.Text + comboBox1.Text;
            using (SqlConnection sqlConnection = new SqlConnection(connect))
            {
                if (Role == "Риэлтор")
                {
                    sqlCommand = new SqlCommand($"update Риэлторы set Статус_удаления = 'True' where Номер_телефона = '{Phone}' or Email = '{Email}'", sqlConnection);
                    sqlConnection.Open();
                    try
                    {
                        sqlCommand.ExecuteNonQuery();
                        MessageBox.Show("Пользователь удалён");
                        autorization = new Autorization();
                        this.Hide();
                        autorization.Show();
                    }catch
                    {
                        MessageBox.Show("Произошли непредвиденные обстоятельства!");
                    }
                    
                }
                if (Role == "Клиент")
                {
                    sqlCommand = new SqlCommand($"update Клиенты set Статус_удаления = 'True' where Номер_телефона = '{Phone}' or Email = '{Email}'", sqlConnection);
                    sqlConnection.Open();
                    try
                    {
                        sqlCommand.ExecuteNonQuery();
                        MessageBox.Show("Пользователь удалён");
                        autorization = new Autorization();
                        this.Hide();
                        autorization.Show();
                    }
                    catch
                    {
                        MessageBox.Show("Произошли непредвиденные обстоятельства!");
                    }

                }
            }
        }
        Autorization autorization;
        private void Account_FormClosed(object sender, FormClosedEventArgs e)
        {
            autorization = new Autorization();
            this.Hide();
            autorization.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            autorization = new Autorization();
            this.Hide();
            autorization.Show();
        }
    }
}
