﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace PracticalProject
{
    public partial class Regisration : Form
    {
        public Regisration()
        {
            InitializeComponent();
        }
        Autorization autorization;
        private void Regisration_FormClosed(object sender, FormClosedEventArgs e)
        {
            autorization = new Autorization();
            this.Hide();
            autorization.Show();
        }
        string connection = ConnectSQL.Connect();
        SqlDataAdapter sqlDataAdapter;
        DataSet dataSet;
        SqlCommand sqlCommand;
        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            char Word = e.KeyChar;
            if (!Char.IsDigit(Word) && Word != 8)
            {
                e.Handled = true;
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.Text == "Клиент")
            {
                label8.Visible = false;
                numericUpDown1.Visible = false;
            }
            if (comboBox2.Text == "Риэлтор")
            {
                label8.Visible = true;
                numericUpDown1.Visible = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            autorization = new Autorization();
            this.Hide();
            autorization.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bool CheckedLoginRielter = false;
            bool CheckedLoginClient = false;
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                if (textBox1.Text != "" || textBox2.Text != "" || textBox3.Text != "" || textBox4.Text != "" || textBox5.Text != "" || comboBox1.SelectedIndex > -1 || textBox6.Text != "" || textBox7.Text != "" || comboBox2.SelectedIndex > -1)
                {
                    if (comboBox2.Text == "Клиент")
                    {

                        if (textBox6.Text == textBox7.Text)
                        {
                            sqlDataAdapter = new SqlDataAdapter("select Номер_телефона, Email from Клиенты", sqlConnection);
                            dataSet = new DataSet();
                            sqlDataAdapter.Fill(dataSet);
                            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                            {
                                if (textBox4.Text == dataSet.Tables[0].Rows[i][0].ToString() || textBox5.Text + comboBox1.Text == dataSet.Tables[0].Rows[i][1].ToString())
                                {
                                    CheckedLoginRielter = true;
                                }
                            }
                            sqlDataAdapter = new SqlDataAdapter("select Номер_телефона, Email from Риэлторы", sqlConnection);
                            dataSet = new DataSet();
                            sqlDataAdapter.Fill(dataSet);
                            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                            {
                                if (textBox4.Text == dataSet.Tables[0].Rows[i][0].ToString() || textBox5.Text + comboBox1.Text == dataSet.Tables[0].Rows[i][1].ToString())
                                {
                                    CheckedLoginClient = true;
                                }
                            }
                            if (CheckedLoginRielter == false && CheckedLoginClient == false)
                            {
                                sqlCommand = new SqlCommand($"insert into Клиенты (Фамилия, Имя, Отчество, Номер_телефона, Email, Пароль, Статус_удаления) values (@fam, @imya, @otc, @nom, @mail, @password, @stat)", sqlConnection);
                                sqlCommand.Parameters.AddWithValue("@fam", textBox1.Text);
                                sqlCommand.Parameters.AddWithValue("@imya", textBox2.Text);
                                sqlCommand.Parameters.AddWithValue("@otc", textBox3.Text);
                                sqlCommand.Parameters.AddWithValue("@nom", textBox4.Text);
                                sqlCommand.Parameters.AddWithValue("@mail", textBox5.Text + comboBox1.Text);
                                sqlCommand.Parameters.AddWithValue("@password", textBox6.Text);
                                sqlCommand.Parameters.AddWithValue("@stat", "False");
                                sqlConnection.Open();
                                try
                                {
                                    sqlCommand.ExecuteNonQuery();
                                    MessageBox.Show("Клиент добавлен!");
                                    textBox1.Text = "";
                                    textBox2.Text = "";
                                    textBox3.Text = "";
                                    textBox4.Text = "";
                                    textBox5.Text = "";
                                    textBox6.Text = "";
                                    textBox7.Text = "";
                                    comboBox1.SelectedIndex = -1;
                                    comboBox2.SelectedIndex = -1;
                                    numericUpDown1.Value = 0;
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.ToString());
                                }
                            }
                            else
                            {
                                MessageBox.Show("Номер телефона или почта уже заняты!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Пароли не совпадают!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }


                    if (comboBox2.Text == "Риэлтор")
                    {
                        if (textBox6.Text == textBox7.Text)
                        {
                            sqlDataAdapter = new SqlDataAdapter("select Номер_телефона, Email from Клиенты", sqlConnection);
                            dataSet = new DataSet();
                            sqlDataAdapter.Fill(dataSet);
                            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                            {
                                if (textBox4.Text == dataSet.Tables[0].Rows[i][0].ToString() || textBox5.Text + comboBox1.Text == dataSet.Tables[0].Rows[i][1].ToString())
                                {
                                    CheckedLoginRielter = true;
                                }
                            }
                            sqlDataAdapter = new SqlDataAdapter("select Номер_телефона, Email from Риэлторы", sqlConnection);
                            dataSet = new DataSet();
                            sqlDataAdapter.Fill(dataSet);
                            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                            {
                                if (textBox4.Text == dataSet.Tables[0].Rows[i][0].ToString() || textBox5.Text + comboBox1.Text == dataSet.Tables[0].Rows[i][1].ToString())
                                {
                                    CheckedLoginClient = true;
                                }
                            }
                            if (CheckedLoginRielter == false && CheckedLoginClient == false)
                            {
                                sqlCommand = new SqlCommand($"insert into Риэлторы (Фамилия, Имя, Отчество, Доля_сделки, Номер_телефона, Email, Пароль, Статус_удаления) values (@fam, @imya, @otc, @dol, @nom, @mail, @password, @stat)", sqlConnection);
                                sqlCommand.Parameters.AddWithValue("@fam", textBox1.Text);
                                sqlCommand.Parameters.AddWithValue("@imya", textBox2.Text);
                                sqlCommand.Parameters.AddWithValue("@otc", textBox3.Text);
                                sqlCommand.Parameters.AddWithValue("@dol", numericUpDown1.Value.ToString());
                                sqlCommand.Parameters.AddWithValue("@nom", textBox4.Text);
                                sqlCommand.Parameters.AddWithValue("@mail", textBox5.Text + comboBox1.Text);
                                sqlCommand.Parameters.AddWithValue("@password", textBox6.Text);
                                sqlCommand.Parameters.AddWithValue("@stat", "False");
                                sqlConnection.Open();
                                try
                                {
                                    sqlCommand.ExecuteNonQuery();
                                    MessageBox.Show("Риэлтор добавлен!");
                                    textBox1.Text = "";
                                    textBox2.Text = "";
                                    textBox3.Text = "";
                                    textBox4.Text = "";
                                    textBox5.Text = "";
                                    textBox6.Text = "";
                                    textBox7.Text = "";
                                    comboBox1.SelectedIndex = -1;
                                    comboBox2.SelectedIndex = -1;
                                    numericUpDown1.Value = 0;
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.ToString());
                                }
                            }
                            else
                            {
                                MessageBox.Show("Номер телефона или почта уже заняты!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Пароли не совпадают!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Вы заполнили не все поля!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
