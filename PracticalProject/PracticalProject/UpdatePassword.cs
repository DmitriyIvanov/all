﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace PracticalProject
{
    public partial class UpdatePassword : Form
    {
        string role, fio, ID;
        public UpdatePassword(string ROLE, string FIO, string id)
        {
            InitializeComponent();
            role = ROLE;
            fio = FIO;
            ID = id;
        }
        Account account;
        string connect = ConnectSQL.Connect();
        SqlDataAdapter sqlDataAdapter;
        DataSet dataSet;
        private void button8_Click(object sender, EventArgs e)
        {
            if (role == "Риэлтор")
            {
                if (textBox1.Text == textBox2.Text)
                {
                    using (SqlConnection sqlConnection = new SqlConnection(connect))
                    {
                        sqlDataAdapter = new SqlDataAdapter($"update Риэлторы set Пароль = {textBox1.Text} where Код_риэлтора = {ID}", sqlConnection);
                        dataSet = new DataSet();
                        sqlDataAdapter.Fill(dataSet);
                        MessageBox.Show("Пароль изменён!");
                        account = new Account(role, fio);
                        this.Hide();
                        account.Show();
                    }
                }
                else
                {
                    MessageBox.Show("Пароли не совпадают!");
                }
            }
            if (role == "Клиент")
            {
                if (textBox1.Text == textBox2.Text)
                {
                    using (SqlConnection sqlConnection = new SqlConnection(connect))
                    {
                        sqlDataAdapter = new SqlDataAdapter($"update Клиенты set Пароль = {textBox1.Text} where Код_клиента = {ID}", sqlConnection);
                        dataSet = new DataSet();
                        sqlDataAdapter.Fill(dataSet);
                        MessageBox.Show("Пароль изменён!");
                        account = new Account(role, fio);
                        this.Hide();
                        account.Show();
                    }
                }
                else
                {
                    MessageBox.Show("Пароли не совпадают!");
                }
            }
        }

        private void UpdatePassword_FormClosed(object sender, FormClosedEventArgs e)
        {
            account = new Account(role, fio);
            this.Hide();
            account.Show();
        }
    }
}
